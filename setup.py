from setuptools import setup, find_packages

with open('requirements.txt') as fp:
    install_requires = fp.read()


setup(
    name = 'niftiSeg',
    version = '0.1.0',
    url = '',
    description = '',
    packages = find_packages(),
    install_requires = install_requires,

    dependency_links=[
        # Make sure to include the `#egg` portion so the `install_requires` recognizes the package
        'git+ssh://git@github.com/farid67/niftiSeg.git#egg=nitfiSeg-0.1'
    ]

)
