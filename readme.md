# Installation

*Requires python 3.5 or above*

### With a python virtual environment

`cd <virtualEnvBaseDir>`

`python3 -m venv niftiSeg-virtEnv`

`source niftiSeg-virtEnv/bin/activate`

### Installation steps

`cd <gitSrcBaseDir>`

`git clone https://gitlab.com/farid67/niftiSeg.git`

`cd niftiSeg`

`pip3 install -r requirements.txt`



# Models download

***Use provided password***

Liver Model

https://owncloud.ircad.fr/index.php/s/99WKRsuGI8gPanS

Tumor Model

https://owncloud.ircad.fr/index.php/s/vgtFrradL360cSc

Necrosis Model

https://owncloud.ircad.fr/index.php/s/ySnu0InGuCvMq0T



# Run prediction

* Liver only 
  * `./niftiSeg.py <pathToFile.nii.gz> <pathToLiverModel.h5>` 
* Liver tissues segmentation
  * generate 3 different volumes : `./niftiSeg.py <pathToFile.nii.gz> <pathToLiverModel.h5> --tumor <pathToTumorModel.h5> --necrosis <pathToNecrosisModel.h5>`
  * fuse prediction in a single volume : `./niftiSeg.py <pathToFile.nii.gz> <pathToLiverModel.h5> --tumor <pathToTumorModel.h5> --necrosis <pathToNecrosisModel.h5> -f`
* Options :
  * the output directory can also be specified using the option `-o/--outputDirectory` 