#!/usr/bin/python3

import argparse
import cv2
import gc
import keras
from keras.models import load_model
import nibabel
import numpy as np
import matplotlib.pyplot as plt
import os
from pyquaternion import Quaternion
from scipy.ndimage.interpolation import affine_transform
import sys
import tensorflow as tf

#####################
# Tf Custom objects
##################### 

def cross_entropy_loss(y_true, y_pred):
    # Notation used from paper : Fully Convolutional Architectures for Multi-Class Segmentation in Chest Radiographs

    # manual computation of crossentropy
    # clk corresponds to the number of pixels belonging to the channel l in the batch k
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk = tf.clip_by_value(clk, 1., ck)
    _epsilon = tf.convert_to_tensor(1e-7, y_pred.dtype.base_dtype)
    y_pred = tf.clip_by_value(y_pred, _epsilon, 1. - _epsilon)
    return -tf.reduce_sum(ck/clk * tf.reduce_mean(y_true * tf.log(y_pred), axis=1), axis=1)

def dice_loss(y_true, y_pred):
    """
        Compute the dice loss
    """
    # clk corresponds to the number of pixels belonging to the channel l in the batch k
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk = tf.clip_by_value(clk, 1., ck)
    dice = 2 * tf.reduce_sum(y_true * y_pred, axis=1)/tf.reduce_sum(y_true + y_pred, axis=1)
    return -tf.reduce_sum(ck/clk * dice, axis=1)

def dice_acc(y_true, y_pred):
    """
        Compute the dice accuracy
    """
    clk = tf.reduce_sum(tf.reduce_sum(y_true, axis=1), axis=0)
    # ck corresponds to the total number of pixels in the batch k
    ck = tf.reduce_sum(clk)
    clk_clipped = tf.clip_by_value(clk, 1., ck)
    y_pred_arg_max = tf.one_hot(tf.argmax(y_pred, 2), y_pred.shape[2])
    intersection = tf.reduce_sum(tf.reduce_sum(y_true * y_pred_arg_max, axis = [1]), axis=0)
    dice = 2 * intersection / (tf.reduce_sum(y_true, axis=[0, 1]) +  tf.reduce_sum(y_pred_arg_max, axis = [0, 1]))
    # Nan on the dice correspond to case where we have neither gt nor prediction data so the union of the two sets is empty
    # We replace those case by a -1 on the dice computation
    dice = tf.where(tf.is_nan(dice), tf.zeros_like(dice), dice)
    unclipped_weights = 1./clk
    sum_weights =  tf.reduce_sum(tf.where(tf.is_inf(unclipped_weights), tf.zeros_like(unclipped_weights), unclipped_weights), axis=0)
    return tf.tensordot((1/clk_clipped), tf.where(tf.is_nan(dice), tf.zeros_like(dice), dice), 1) / sum_weights

##########################
# Utils
##########################

def proba2OneHot(prediction, image_size=512, target="liver"):
    """
        Convert model prediction to binary output, 0 = background, 1 = target class
        Args :
            prediction (numpy.ndarray): network output prediction
            image_size (int): returned image size (default to 512)
            target (str): target class
        Returns:
            out (numpy.ndarray): binary output, shape (image_size, image_size, 1)
    """
    n_classes = 3
    if target == "liver":
        n_classes = 1
    predicted_image = np.reshape(prediction, (image_size, image_size, n_classes))
    out = np.zeros(((image_size, image_size, 1)))
    if n_classes == 1:
        out[predicted_image[:, :, 0] > 0.5] = 1
    else: # target = "tumor" or "liver" : 
        out[predicted_image[:, :, 2] > 0.5] = 1
    return np.squeeze(out)

def transform_image(input_image, input_spacing, largest_spacing):
    """
        Transform the image to have the size and the spacing required by the network
        Args :
            input_image (numpy.ndarray): image to transform
            input_spacing (float): input image spacing
            largest_spacing (float): spacing used in images used to train the network
        Returns:
            out_image (numpy.ndarray): transformed image
    """
    spacing_scale = largest_spacing/input_spacing
    # print ("spacing scale : ", spacing_scale)
    transfo_matrix = np.eye(2) * spacing_scale
    output_shape = [int(np.ceil(input_image.shape[i]/spacing_scale)) for i in range(2)]
    out_image = affine_transform(input_image, transfo_matrix, output_shape=output_shape, order=0, mode="nearest")
    return out_image


def mask(input_image, annotated_image):
    """
        Function to mask the input_image in order to fit with the network requirements (e.g. when segmenting the lesions, all non-liver pixels have to be masked)
        Args:
            input_image (numpy.ndarray): image to mask
            annotated_image (numpy.ndarray): binary image used to mask the input image, should normally correspond to previous segmentation in the cascade
        Returns:
            out_image (numpy.ndarray): The masked image
    """
    out_image = np.copy(input_image)
    out_image[annotated_image == 0] = -1
    return out_image


def fuseMasks(liverBinary, tumorBinary, necrosisBinary):
    """
        Create a multilabel output map with 4 classes, the background, the parenchyma, and both the active and the necrosis part of the lesions
        Args:
            liverBinary (numpy.ndarray): image corresponding to the binary segmentation of the liver
            tumorBinary (numpy.ndarray): image corresponding to the binary segmentation of the tumor
            necrosisBinary (numpy.ndarray): image corresponding to the binary segmentation of the necrosis
        Returns:
            out_image (numpy.ndarray): multilabel segmentation map
    """
    out = np.zeros((liverBinary.shape[0], liverBinary.shape[1], liverBinary.shape[2]))
    out[liverBinary == 1] = 1
    out[tumorBinary == 1] = 2
    out[necrosisBinary == 1] = 3
    return out


##########################
# Main pred function
##########################

def segmentNifti (inputFile, modelLiverPath, modelTumorPath=None, modelNecrosisPath=None, outputDir=None, fuse=False):
    """ 
        Convert nifti volume to numpy then run prediction (Liver, Tumor and Necrosis if specified as arg) and convert back to nifti by preserving geometrical properties
        Args:
            inputFile (str): path to the input image
            modelLiverPath (str): path to the model used to segment the liver within the whole CT
            modelTumorPath (str): path to the model used to segment the tumor within the liver (default to None)
            modelNecrosisPath (str): path to the model used to segment the necrosis within the lesions (default to None)
            outputDir (str): path used as prefix to store the created nifti volume
        Returns :
            None : Create only the different nifti volumes
    """
    nifti_data = nibabel.load(inputFile)
    pixel_array = nifti_data.get_fdata()
    spacing = nifti_data.header.get("pixdim")[1] # WARNING : Take only the first value of spacing : because spacing for x & y is the same and we do not consider z-spacing
    if nifti_data.header.has_data_slope and nifti_data.header.has_data_intercept and nifti_data.header.get_slope_inter() != (None, None):
        pixel_array = pixel_array*nifti_data.header.get_slope_inter()[0] + nifti_data.header.get_slope_inter()[1]

    rotation_matrix = Quaternion(nifti_data.header.get_qform_quaternion()).rotation_matrix

    liver_out_pixel_array = np.zeros(pixel_array.shape)

    ### TRANSFORMATION TO NUMPY WORLD (NEED Transpose After)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            pixel_array = np.flip(pixel_array, axis=ax)

    print ("Liver segmentation...\n")
    ### perform the liver segmentation here
    model = load_model(modelLiverPath, custom_objects={"cross_entropy_loss":cross_entropy_loss, "dice_loss":dice_loss, "dice_acc": dice_acc})
    min_range = -100.
    max_range = 400.
    f_linFun = lambda x: (x-min_range)/(max_range-min_range)

    for slice_ in range(pixel_array.shape[2]):
        sys.stdout.write("{}/{}\r".format(slice_+1,pixel_array.shape[2]))
        sys.stdout.flush()
        transformed_image = cv2.resize(transform_image(pixel_array[:,:, slice_].T, spacing, 0.87), (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST) 
        X = np.array([f_linFun(transformed_image)])
        out = proba2OneHot(model.predict(X[:, np.newaxis, :, :]), image_size=model.input.shape[-1])
        liver_out_pixel_array[:, :, slice_] = cv2.resize(transform_image(out, 0.87, spacing).T, (pixel_array.shape[1], pixel_array.shape[0]) , interpolation=cv2.INTER_NEAREST)
    print ("\n")
    del model
    gc.collect()
    ### end liver segmentation 


    ### Perform additional segmentation (Tumor and necrosis if specified)
    if modelTumorPath != None:
        print ("Tumor segmentation...\n")
        tumor_out_pixel_array = np.zeros(pixel_array.shape)

        model = load_model(modelTumorPath, custom_objects={"cross_entropy_loss":cross_entropy_loss, "dice_loss":dice_loss, "dice_acc": dice_acc})

        for slice_ in range(pixel_array.shape[2]):
            sys.stdout.write("{}/{}\r".format(slice_+1,pixel_array.shape[2]))
            sys.stdout.flush()

            transformed_image = cv2.resize(transform_image(pixel_array[:,:, slice_].T, spacing, 0.87), 
                (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
            transformed_annotated_image = cv2.resize(transform_image(liver_out_pixel_array[:,:, slice_].T, spacing, 0.87), 
                (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
            X = np.array([mask(f_linFun(transformed_image), transformed_annotated_image)])
            out = proba2OneHot(model.predict(X[:, np.newaxis, :, :]), image_size=model.input.shape[-1], target="tumor")
            tumor_out_pixel_array[:, :, slice_] = cv2.resize(transform_image(out, 0.87, spacing).T, (pixel_array.shape[1], pixel_array.shape[0]) , interpolation=cv2.INTER_NEAREST)
        print ("\n")
        del model
        gc.collect()

        if modelNecrosisPath != None:
            print ("Necrosis segmentation...\n")
            necrosis_out_pixel_array = np.zeros(pixel_array.shape)

            model = load_model(modelNecrosisPath, custom_objects={"cross_entropy_loss":cross_entropy_loss, "dice_loss":dice_loss, "dice_acc": dice_acc})

            for slice_ in range(pixel_array.shape[2]):
                sys.stdout.write("{}/{}\r".format(slice_+1,pixel_array.shape[2]))
                sys.stdout.flush()

                transformed_image = cv2.resize(transform_image(pixel_array[:,:, slice_].T, spacing, 0.97), 
                    (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
                transformed_annotated_image = cv2.resize(transform_image(tumor_out_pixel_array[:,:, slice_].T, spacing, 0.97), 
                    (model.input.shape[-1], model.input.shape[-1]), interpolation=cv2.INTER_NEAREST)
                X = np.array([mask(f_linFun(transformed_image), transformed_annotated_image)])
                out = proba2OneHot(model.predict(X[:, np.newaxis, :, :]), image_size=model.input.shape[-1], target="tumor")
                necrosis_out_pixel_array[:, :, slice_] = cv2.resize(transform_image(out, 0.97, spacing).T, (pixel_array.shape[1], pixel_array.shape[0]) , interpolation=cv2.INTER_NEAREST)

            print ("\n")
            del model
            gc.collect()
    ### End / Perform additional segmentation (Tumor and necrosis if specified)




    ### retransform the output array, by transposing back, flipping, and rescale the data (slope&intercept)
    for ax in range(2):
        if rotation_matrix[ax,ax] == 1.:
            liver_out_pixel_array = np.flip(liver_out_pixel_array, axis=ax)
            if modelTumorPath != None:
                tumor_out_pixel_array = np.flip(tumor_out_pixel_array, axis=ax)
            if modelNecrosisPath != None:
                necrosis_out_pixel_array = np.flip(necrosis_out_pixel_array, axis=ax)

    ### Create dir and files 
    if not os.path.exists(os.path.abspath(outputDir)):
        os.mkdir(os.path.abspath(outputDir))

    ### Get the input filename to have more spccific output name
    fname = os.path.basename(inputFile).split(".")[0] 


    if fuse : 
        out_nifti_data = nibabel.nifti1.Nifti1Image(fuseMasks(liver_out_pixel_array, tumor_out_pixel_array, necrosis_out_pixel_array), affine=nifti_data.get_affine(), header=nifti_data.header.copy())
        out_nifti_data.header.set_slope_inter(None, None)
        nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_liverTissues.nii.gz"))
    else :
        ### Here convert numpy volume to nifti back
        out_nifti_data = nibabel.nifti1.Nifti1Image(liver_out_pixel_array, affine=nifti_data.get_affine(), header=nifti_data.header.copy())
        out_nifti_data.header.set_slope_inter(None, None)
        nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_liver.nii.gz"))
        if modelTumorPath != None:
            out_nifti_data = nibabel.nifti1.Nifti1Image(tumor_out_pixel_array, affine=nifti_data.get_affine(), header=nifti_data.header.copy())
            out_nifti_data.header.set_slope_inter(None, None)
            nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_tumor.nii.gz"))
        if modelNecrosisPath != None:
            out_nifti_data = nibabel.nifti1.Nifti1Image(necrosis_out_pixel_array, affine=nifti_data.get_affine(), header=nifti_data.header.copy())
            out_nifti_data.header.set_slope_inter(None, None)
            nibabel.save(out_nifti_data, os.path.join(outputDir, fname+"_necrosis.nii.gz"))



if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("niftiInputPath", help="Path to nifti file to segment", action="store")
    parser.add_argument("modelLiverPath", help="model to use for the liver prediction", action="store")
    parser.add_argument("--tumor", help="model to use for the tumor prediction", action="store", dest="modelTumorPath", default=None)
    parser.add_argument("--necrosis", help="model to use for the necrosis prediction", action="store", dest="modelNecrosisPath", default=None)
    parser.add_argument("-o", "--outputDirectory", help="directory where to store the .npy files", action="store", dest="outputDir", default=os.path.abspath(os.curdir))
    parser.add_argument("-f", "--fuse", help="option to fuse the 3 binary segmentation masks into a single", action="store_true", dest="fuseMap", default=False)

    args = parser.parse_args()

    segmentNifti(args.niftiInputPath, args.modelLiverPath, args.modelTumorPath, args.modelNecrosisPath, args.outputDir, args.fuseMap)

